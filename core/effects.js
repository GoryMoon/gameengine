Classes.Effects = Class.create({
	
	/**
	 * Define toggle values and initialize day
	 */
	initialize: function()
	{
		this.isNight = false;
		this.isRaining = false;
		this.isSnowing = false;
		this.isEarthquake = false;
		this.isThunder = false;

		this.Day.initialize();
	},

	/**
	 * Tick the effect that is running
	 */
	tick: function()
	{
		if(this.isNight)
		{
			this.Night.tick();
		}
		else if(this.isRaining)
		{
			this.Rain.tick();
		}
		else if(this.isSnowing)
		{
			this.Snow.tick();
		}
		else
		{
			this.Day.tick();
		}
	},

	/**
	 * Toggle day or night state (also changes light level)
	 */
	toggleNight: function()
	{
		var bg = document.getElementById("canvases");
		
		this.isNight = (!this.isNight) ? true : false;

		if(this.isNight)
		{
			bg.style.backgroundColor = "rgb(0, 0, 0)";
			Game.Effects.setLight(6);
			Game.Effects.Night.initialize();
		}
		else
		{
			bg.style.backgroundColor = "rgb(0, 186, 255)";
			Game.Effects.setLight(10);
			Game.Effects.Day.initialize();
		}
	},

	/**
	 * Change light level
	 * @param Int level 1-10
	 */
	setLight: function(level)
	{
		document.getElementById("canvases").style.opacity = level / 10;
	},

	/**
	 * Toggle rain animation
	 */
	toggleRain: function()
	{
		var bg = document.getElementById("canvases");

		this.isRaining = (!this.isRaining) ? true : false;

		if(this.isRaining)
		{
			this.Rain.initialize();
			bg.style.backgroundColor = "rgb(96, 113, 126)";
			Game.Effects.setLight(6);
		}
		else
		{
			Renderers.Entities.cleared = true;
			bg.style.backgroundColor = "rgb(0, 186, 255)";
			Game.Effects.setLight(10);
		}

		var bg = document.getElementById("canvases");
	}, 

	/**
	 * Toggle snow animation
	 */
	toggleSnow: function()
	{
		var bg = document.getElementById("canvases");

		this.isSnowing = (!this.isSnowing) ? true : false;

		if(this.isSnowing)
		{
			this.Snow.initialize();
			bg.style.backgroundColor = "rgb(166, 182, 195)";
			Game.Effects.setLight(6);
		}
		else
		{
			Renderers.Entities.cleared = true;
			bg.style.backgroundColor = "rgb(0, 186, 255)";
			Game.Effects.setLight(10);
		}

		var bg = document.getElementById("canvases");
	},

	/**
	 * Toggle earthquake animation (freezes player)
	 */
	toggleEarthquake: function()
	{
		this.isEarthquake = (!this.isEarthquake) ? true : false;

		if(this.isEarthquake)
		{
			this.Earthquake.initialize();
		}
		else
		{
			clearInterval(this.Earthquake.interval);

			Game.x = this.Earthquake.gameX;
			Game.Player.staticX = this.Earthquake.staticX;
			Game.y = this.Earthquake.gameY;
			Game.Player.staticY = this.Earthquake.staticY;
		}
	},

	/**
	 * Toggle thunder animation
	 */
	toggleThunder: function()
	{
		this.isThunder = (!this.isThunder) ? true : false;

		if(this.isThunder)
		{
			this.Thunder.initialize();
		}
		else
		{
			clearInterval(this.Thunder.interval);
		}
	},

	/**
	 * Rain handler object
	 */
	Rain: {
		particles: [],

		/**
		 * Initialize rain particles
		 */
		initialize: function()
		{
			if(!this.particles.length)
			{
				var x, y;

				for(var i = 0; i < 500; i++)
				{
					x = Math.floor((Math.random() * 800) + 1);
					y = Math.floor(Math.random() * -480);
					this.particles.push(new Entities.Particle(x,y,-1,6,"rgba(255,255,255,0.5)", 2, 4));
				}
			}
		},

		/**
		 * Move the particles
		 */
		tick: function()
		{
			if(this.particles)
			{
				this.particles.forEach(function(particle)
				{
					particle.tick();
				});
			}
		}
	},

	/**
	 * Night handler object
	 */
	Night: {
		particles: [],

		/**
		 * Initialize stars
		 */
		initialize: function()
		{
			if(!this.particles.length)
			{
				var x, y;

				for(var i = 0; i < 100; i++)
				{
					x = Math.floor((Math.random() * 800) + 1);
					y = Math.floor(Math.random() * 480);
					this.particles.push([x, y]);
				}
			}
		},

		/**
		 * Draw the stars
		 */
		tick: function()
		{
			if(this.particles)
			{
				this.particles.forEach(function(particle)
				{
					Renderers.Background.queue(function(context)
					{
						context.fillStyle = "rgb(255,255,255)";
						context.fillRect(particle[0], particle[1], 2, 2);
					});
				});
			}
		}
	},

	/**
	 * Day handler object
	 */
	Day: {
		
		clouds: [],

		/**
		 * Initialize cloud objects
		 */
		initialize: function()
		{
			if(!this.clouds.length)
			{
				var x = y = 0;

				for(var i = 0; i < 5; i++)
				{
					x = Math.floor((Math.random() * 800) + 1);
					y = Math.floor(Math.random() * 200);
					this.clouds.push(new Entities.Cloud(x,y,0.5,0));
				}
			}
		},

		/**
		 * Move clouds
		 */
		tick: function()
		{
			if(this.clouds)
			{
				this.clouds.forEach(function(cloud)
				{
					cloud.tick();
				});
			}
		}
	},

	/**
	 * Snow handler object
	 */
	Snow: {
		
		particles: [],

		/**
		 * Initialize snow particles
		 */
		initialize: function()
		{
			if(!this.particles.length)
			{
				var x, y;

				for(var i = 0; i < 300; i++)
				{
					x = Math.floor((Math.random() * 800) + 1);
					y = Math.floor(Math.random() * -480);
					this.particles.push(new Entities.Particle(x,y,0,2,"rgb(255,255,255)", 3, 3));
				}
			}
		},

		/**
		 * Move particles
		 */
		tick: function()
		{
			if(this.particles)
			{
				this.particles.forEach(function(particle)
				{
					particle.tick();
				});
			}
		}
	},

	/**
	 * Earthquake handler object
	 */
	Earthquake: {
		
		interval: false,
		gameX: false,
		gameY: false,
		staticX: false,
		staticY: false,

		/**
		 * Initialize the earthquake interval
		 */
		initialize: function()
		{
			this.gameX = Game.x;
			this.staticX = Game.Player.staticX;
			this.gameY = Game.y;
			this.staticY = Game.Player.staticY;

			var add = false;

			this.interval = setInterval(function()
			{
				add = (add) ? false : true;
				Game.x += (add) ? 0.16 : -0.16;
				Game.Player.staticX += (add) ? 0.16 : -0.16;
				Game.y += (add) ? 0.08 : -0.08;
				Game.Player.staticY += (add) ? 0.08 : -0.08;


				Renderers.Entities.cleared = true;
			}, 80);
		}
	},

	/**
	 * Thunder handler object
	 */
	Thunder: {
		
		interval: false,

		/**
		 * Initialize the thunder interval
		 */
		initialize: function()
		{
			this.interval = setInterval(Game.Effects.Thunder.trigger, 5000);
		},

		opacity: 0,
		up: true,
		times: 0,

		trigger: function()
		{
			setTimeout(function()
			{
				Game.Effects.Thunder.times++;

				if(Game.Effects.Thunder.times <= 8)
				{

					Renderers.Entities.queue(function(context)
					{
						if(Game.Effects.Thunder.opacity >= 1)
						{
							Game.Effects.Thunder.up = false;
						}
						else if(Game.Effects.Thunder.opacity <= 0)
						{
							Game.Effects.Thunder.up = true;
						}

						if(Game.Effects.Thunder.up)
						{
							Game.Effects.Thunder.opacity += 0.25;
						}
						else
						{
							Game.Effects.Thunder.opacity -= 0.25;
						}

						context.fillStyle = "rgba(255,255,255," + Game.Effects.Thunder.opacity + ")";
						context.fillRect(0,0,800,480);
						Renderers.Entities.cleared = true;
					});

					Game.Effects.Thunder.trigger();
				}
				else
				{
					Game.Effects.Thunder.times = 0;
					Game.Effects.Thunder.opacity = 0;
				}
			}, 25);
		}
	},
});