Classes.Renderer = Class.create({
	
	/**
	 * Attach the canvas element to the class
	 * @param String id
	 */
	initialize: function(id)
	{
		this.id = id;

		this.canvas = document.getElementById(id);
		this.context = this.canvas.getContext("2d");

		Log.notice(id + " renderer started");

		this.cleared = true;

		this.toDraw = [];
	},

	/**
	 * Draw all necessary objects
	 */
	draw: function()
	{
		if(this.toDraw.length)
		{
			this.clear();

			var self = this;

			this.toDraw.forEach(function(method)
			{
				method(self.context);
			});

			this.toDraw = [];

			this.cleared = false;
		}
	},

	/**
	 * Queue up drawing methods to the renderer
	 * @param Function method
	 */
	queue: function(method)
	{
		this.cleared = true;
		this.toDraw.push(method);
	},

	/**
	 * Clear the canvas
	 */
	clear: function()
	{
		this.cleared = true;
		
		this.context.clearRect(0,0,800,480);
	}
});