Classes.Resources = Class.create({
	
	initialize: function()
	{
		this.images = {};
		this.isLoaded = {};

		this.load("overlay", "overlay.png");
		this.load("grass", "grass.png");
		this.load("dirt", "dirt.png");
		this.load("stone", "stone.png");
		this.load("sand", "sand.png");
		this.load("wood", "wood.png");
		this.load("leaves", "leaves.png");
		this.load("glass", "glass.png");
	},

	/**
	 * Load an image
	 * @param String name
	 * @param String file
	 * @param Function callback
	 */
	load: function(name, file)
	{
		this.isLoaded[name] = false;
		this.images[name] = new Image();
		this.images[name].src = "resources/" + file;
		this.images[name].onload = function()
		{
			Log.notice("resources/" + file + " has been loaded");
			Game.Resources.isLoaded[name] = true;
		};
	}
});