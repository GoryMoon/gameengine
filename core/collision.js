Classes.Collision = Class.create({
	
	/**
	 * Test if object collides with any block
	 * @param Object object
	 * @param Array blocks
	 * @return Boolean
	 */
	test: function(object, direction)
	{
		var block = false;

		switch(direction)
		{
			case "up":
				if(object.y <= Math.round(object.y))
				{
					block = Game.getTile(Math.round(object.x), Math.round(object.y - 1));
				}
			break;

			case "left":
				if(object.x <= Math.round(object.x))
				{
					block = Game.getTile(Math.round(object.x - 1), Math.round(object.y));
				}
			break;

			case "right":
				if(object.x >= Math.round(object.x))
				{
					block = Game.getTile(Math.round(object.x + 1), Math.round(object.y));
				}
			break;

			case "down":
				if(object.y >= Math.round(object.y))
				{
					block = Game.getTile(Math.round(object.x), Math.round(object.y + 1));
				}
			break;
		}

		return (block) ? true : false;
	}
});