Classes.Game = Class.create({
	
	/**
	 * Initialize the game
	 */
	initialize: function()
	{
		this.Config = {
			FPS: 80,
			jumpHeight: 2,
			jumpSpeed: 0.2,
			fallSpeed: 0.2,
			jumps: 2,
			debug: true
		};

		this.x = 0;
		this.y = 0;

		this.Input = new Classes.Input();
		this.Collision = new Classes.Collision();
		this.World = new Classes.World();
		this.Resources = new Classes.Resources();
		this.Effects = new Classes.Effects();
		
		if(this.Config.debug)
		{
			this.DevTools = new Classes.DevTools();
		}

		this.Player = new Entities.Player(Renderers.Entities, 7, 11);
		this.UI = new Classes.Interface();

		this.entities = [];
		this.tiles = [];
		this.overlay = [];
		this.placedBlock = false;
	},

	start: function()
	{
		// Load the level data
		Game.World.load("1", function()
		{
			// Create the game loop
			Main.Loop = new Classes.Loop(Game.Config.FPS);
			Main.Loop.start();
		});
	},

	/**
	 * Get the entity on the specific tile
	 * @param Int x
	 * @param Int y
	 * @return Object
	 */
	getTile: function(x, y)
	{
		if(!this.tiles.length)
		{
			this.entities.forEach(function(entity)
			{
				entityX = Math.round(entity.x);
				entityY = Math.round(entity.y);

				if(!Game.tiles[entityX])
				{
					Game.tiles[entityX] = [];
				}

				Game.tiles[entityX][entityY] = entity;
			});
		}

		try
		{
			return (this.tiles[x][y].solid) ? this.tiles[x][y] : false;
		}
		catch(error)
		{
			return false;
		}
	},

	/**
	 * Game actions
	 */
	tick: function()
	{
		var coords = translate(this.x, this.y);

		this.realX = coords.x;
		this.realY = coords.y;

		this.visibleXMin = Math.round(this.Player.x-8);
		this.visibleXMax = this.visibleXMin + 26;
		
		this.visibleYMin = Math.round(this.Player.y-12);
		this.visibleYMax = this.visibleYMin + 16;

		if(this.Config.debug)
		{
			this.DevTools.tick();
		}

		this.Effects.tick();
		this.Player.tick();
		this.UI.tick();
	},

	/**
	 * Check if block is in range of visible blocks
	 * @param Int x
	 * @param Int y
	 * @return Boolean
	 */
	isVisible: function(x, y)
	{
		if(inRange(x, this.visibleXMin, this.visibleXMax)
		&& inRange(y, this.visibleYMin, this.visibleYMax))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
});