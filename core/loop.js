Classes.Loop = Class.create({
	
	/**
	 * Define values
	 * @param Int rate
	 */
	initialize: function(rate)
	{
		// Initialize variables
		this.frames = 0;
		this.lastFrame = 0;
		this.lastFpsFrame = 0;

		// Define values
		this.delay = Math.round((1/rate) * 1000);
		this.fpsElement = document.getElementById("fps_value");
	},

	/**
	 * Start/resume the game loop
	 */
	start: function()
	{
		Log.notice("Loop started");
		this.isRunning = true;
		this.frame();
	},

	/**
	 * Stop/pause the game loop
	 */
	stop: function()
	{
		Log.notice("Loop stopped");
		this.isRunning = false;
		this.fpsElement.innerHTML = "0";
	},

	/**
	 * Pause or resume
	 * @param Object element
	 */
	toggle: function(element)
	{
		if(this.isRunning)
		{
			this.stop();
			element.innerHTML = "Resume";
		}
		else
		{
			this.start();
			element.innerHTML = "Pause";
		}
	},

	/**
	 * Perform frame tasks
	 */
	frame: function()
	{
		if(this.isRunning)
		{
			var self = this;

			requestAnimFrame(function()
			{
				self.frame();
			});

			var time = new Date().getTime();

			if(time >= this.lastFrame + this.delay)
			{
				this.getFps(time);

				this.difference = time - this.lastFrame;
				this.lastFrame = time;

				this.tick();
				this.draw();
			}
		}
	},

	/**
	 * Count the current FPS based on the count of passed frames
	 * @param Int time
	 */
	getFps: function(time)
	{
		this.frames++;

		if(time >= this.lastFpsFrame + 1000)
		{
			var fps = this.frames;

			this.lastFpsFrame = time;
			this.frames = 1;
			this.fpsElement.innerHTML = fps;
		}
	},

	/**
	 * Update the game logic
	 */
	tick: function()
	{
		Game.tick();

		if(Game.entities.length)
		{
			Game.entities.forEach(function(object)
			{
				if(typeof(object.tick) != "undefined")
				{
					object.tick();
				}
			});
		}
	},

	/**
	 * Draw the game
	 */
	draw: function()
	{
		Renderers.Background.draw();
		Renderers.Foreground.draw();
		Renderers.Entities.draw();
	}
});