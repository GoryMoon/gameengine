Classes.DevTools = Class.create({
	
	initialize: function()
	{
		document.getElementById("dev").style.display = "block";

		this.Renderer = new Classes.Renderer("dev");
		
		this.grid = false;
		this.editor = false;
	},

	tick: function()
	{
		if(this.grid)
		{
			this.drawGrid();
		}

		this.Renderer.draw();
	},

	drawEditor: function()
	{	

		for (var i = 0; i < Game.entities.length; i++) {
			if (Game.entities[i].type == -2)
				Game.entities[i] = "";
		};

		Game.placedBlock = false;
		Game.overlay = [];

		var x = Math.round(Game.Input.mouseX + Game.Player.x - 8);
        var y = Math.round(Game.Input.mouseY + Game.Player.y - 12);

        if(!Game.overlay[x])
		{
			Game.overlay[x] = [];
		}

		if(Game.overlay[x][y])
		{
			Game.overlay[x][y].solid = false;
			Game.overlay[x][y].type = -2;
			Game.overlay[x][y].color = false;
		}
		else
		{
			Game.entities.push(new Entities["Block"](Renderers.Background, x, y, false, -2));
			Game.overlay[x][y] = Game.entities[Game.entities.length - 1];
		}
	},

	drawGrid: function()
	{
		this.Renderer.queue(function(context)
		{
			for(var x = Game.visibleXMin; x < Game.visibleXMax; x++)
			{
				for(var y = Game.visibleYMin; y < Game.visibleYMax; y++)
				{
					context.strokeStyle = "rgba(0,0,0,0.1)";
					context.lineWidth = 1;

					var coords = translate(x + Game.x, y + Game.y);
					context.strokeRect(coords.x, coords.y, tileSize, tileSize);
				}
			}
		});
	},

	/**
	 * Toggle editor
	 * @param Object element
	 */
	toggleEditor: function(element)
	{
		if(this.editor)
		{
			this.grid = false;

			Game.Input.clickListener = false;
			this.Editor.save();
			this.editor = false;
			this.Renderer.clear();
			element.innerHTML = "Edit level";
			document.getElementById("editor").style.display = "none";
		}
		else
		{
			this.grid = true;

			this.Editor.initialize();
			document.getElementById("editor").style.display = "block";
			this.editor = true;
			element.innerHTML = "Save level";
		}
	},

	Editor: {

		data: [],

		entity: "Block",
		type: 1,
		solid: 1,

		setType: function(id, field)
		{
			document.querySelector(".active").className = "";
			field.className = "active";

			this.type = id;
		},

		toggleSolid: function(field)
		{
			if(this.solid)
			{
				this.solid = 0;
				field.innerHTML = "Type: decoration";
			}
			else
			{
				this.solid = 1;
				field.innerHTML = "Type: block";
			}
		},

		initialize: function()
		{

			Game.Input.clickListener = function()
			{
				Game.placedBlock = true;
				var x = Math.round(Game.Input.mouseX + Game.Player.x - 8);
                var y = Math.round(Game.Input.mouseY + Game.Player.y - 12);

				if(!Game.tiles[x])
				{
					Game.tiles[x] = [];
				}

				if(Game.tiles[x][y])
				{
					Game.tiles[x][y].solid = Game.DevTools.Editor.solid;
					Game.tiles[x][y].type = Game.DevTools.Editor.type;
					Game.tiles[x][y].color = false;
				}
				else
				{
					Game.entities.push(new Entities[Game.DevTools.Editor.entity](Renderers.Background, x, y, Game.DevTools.Editor.solid, Game.DevTools.Editor.type));
					Game.tiles[x][y] = Game.entities[Game.entities.length - 1];
				}
			};
		},

		getLevelData: function()
		{
			for(x in Game.tiles)
			{
				for(y in Game.tiles[x])
				{
					if(Game.tiles[x][y].type != -1)
					{
						if(!this.data[x])
						{
							this.data[x] = [];
						}

						this.data[x][y] = [
							Game.tiles[x][y].className,
							Game.tiles[x][y].x,
							Game.tiles[x][y].y,
							(Game.tiles[x][y].solid) ? 1 : 0,
							Game.tiles[x][y].type
						];
					}
				}
			}
		},

		save: function()
		{
			for (var i = 0; i < Game.entities.length; i++) {
				if (Game.entities[i].type == -2)
					Game.entities[i] = "";
			};

			this.getLevelData();

			alert("Saved to Javascript console!");
			console.log(JSON.stringify(this.data));
		}
	}
});