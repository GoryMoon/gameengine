Classes.Interface = Class.create({
	
	initialize: function()
	{
		this.name = null;
		this.level = null;
		this.xp = null;
	},

	/**
	 * Draw the game UI
	 */
	tick: function()
	{
		if(this.level != Game.Player.level || this.xp != Game.Player.xp)
		{
			this.level = Game.Player.level;
			this.xp = Game.Player.xp;

			var self = this;

			Renderers.UI.queue(function(context)
			{
				context.fillStyle = "rgba(0,0,0,0.5)";
				context.fillRect(0, 0, 800, 35);
				//context.fillRect(0, 445, 800, 35);

				for(var i = 0; i < 14; i++)
				{
					if(self.xp >= i + 1)
					{
						context.fillStyle = "rgba(255,255,0,0.8)";
					}
					else
					{
						context.fillStyle = "rgba(255,255,255,0.3)";
					}

					context.fillRect(75+i*51, 10, 50, 15);
				}

				context.fillStyle = "rgba(255,255,255,0.8)";

				context.font = "bold 14px arial";
				context.fillText("Level " + self.level, 10, 22);
			});

			Renderers.UI.draw();
		}
	}
});