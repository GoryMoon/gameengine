Classes.Input = Class.create({
	
	/**
	 * Initialize the event listeners
	 */
	initialize: function()
	{
		this.left = false;
		this.right = false;
		this.jump = false;

		this.mouseX = 0;
		this.mouseY = 0;

		this.clickListener = false;

		var self = this;

		// Key down handler
		window.onkeydown = function(e)
		{
			switch(e.keyCode)
			{
				case Keys.left: self.left = true; break;
				case Keys.right: self.right = true; break;
				case Keys.jump: self.jump = true; break;
			}
		}

		// Key up handler
		window.onkeyup = function(e)
		{
			switch(e.keyCode)
			{
				case Keys.left: self.left = false; break;
				case Keys.right: self.right = false; break;
				case Keys.jump: self.jump = false; break;
			}
		}

		// Click handler
		document.getElementById("canvases").onclick = function(e)
		{
			self.mouseX = Math.ceil((e.pageX - this.offsetLeft) / tileSize);
			self.mouseY = Math.ceil((e.pageY - this.offsetTop) / tileSize);

			if(self.clickListener)
			{
				self.clickListener();
			}

			Log.notice("Clicked at (" + self.mouseX + ", " + self.mouseY + ") which translates to (" + Math.ceil(self.mouseX + Game.Player.x - 7) + "," + Math.ceil(self.mouseY + Game.Player.y - 11) + ")");
		}

		// Mouse handler
		document.getElementById("canvases").onmousemove = function(e)
		{
			var x = Math.ceil((e.pageX - this.offsetLeft) / tileSize);
			var y = Math.ceil((e.pageY - this.offsetTop) / tileSize);

			if(x != self.mouseX || y != self.mouseY)
			{
				
				self.mouseX = x;
				self.mouseY = y;

				if(Game.DevTools.editor)
				{
					Game.DevTools.drawEditor();
				}

				document.getElementById("coordinates").innerHTML = "(" + Math.ceil(self.mouseX + Game.Player.x - 7) + "," + Math.ceil(self.mouseY + Game.Player.y - 11) + ")";
			}
		}
	}
});