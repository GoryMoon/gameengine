Classes.Main = Class.create({

	/**
	 * Initialize the game engine
	 */
	initialize: function()
	{
		Log = new Classes.Log();

		// Initialize renderer objects
		Renderers.Background = new Classes.Renderer("background");
		Renderers.Foreground = new Classes.Renderer("foreground");
		Renderers.Entities = new Classes.Renderer("entities");
		Renderers.UI = new Classes.Renderer("ui");

		Game = new Classes.Game();
	}
});