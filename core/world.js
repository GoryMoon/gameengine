Classes.World = Class.create({
	data: [],

	load: function(level, callback)
	{
		var self = this;

		Log.notice("Loading level file...");

		new Ajax.Request("levels/" + level + ".js", {
			method: "get",
			onSuccess: function(transport)
			{
				Log.notice("Level file loaded");
				self.process(transport.responseText, callback);
			}
		});
	},

	process: function(data, callback)
	{
		var blocks = 0;

		Log.notice("Processing level data");

		this.data = JSON.parse(data);

		this.data.forEach(function(x)
		{
			if(x)
			{
				x.forEach(function(tile)
				{
					if(tile)
					{
						blocks++;
						Game.entities.push(new Entities[tile[0]](Renderers.Background, tile[1], tile[2], tile[3], tile[4]));
					}
				});
			}
		});
		
		Log.notice("Spawned " + blocks + " blocks");

		callback();
	}
});