Classes.Log = Class.create({
	
	/**
	 * Post a notice level message
	 * @param String message
	 */
	notice: function(message)
	{
		console.log("[Notice] " + message);
	},

	/**
	 * Post a warning level message
	 * @param String message
	 */
	warning: function(message)
	{
		console.log("[Warning] " + message);
	},

	/**
	 * Post an error level message
	 * @param String message
	 */
	error: function(message)
	{
		console.log("[Error] " + message);
	}
});