<?php
	$files = array();

	$files[] = glob("core/*.js");
	$files[] = glob("entities/*.js");
	$files[] = glob("levels/*.js");
	$files[] = glob("config/*.js");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>HTML5 canvas game</title>

		<link rel="stylesheet" href="game.css" type="text/css" />

		<!-- Load libraries -->
		<script type="text/javascript" src="libraries/require.js"></script>
		<script type="text/javascript" src="libraries/prototype.js"></script>
		
		<script type="text/javascript">
			// Cross-browser compability
			window.requestAnimFrame = (function(){
				return window.requestAnimationFrame || 
				window.webkitRequestAnimationFrame  || 
				window.mozRequestAnimationFrame     || 
				window.oRequestAnimationFrame       || 
				window.msRequestAnimationFrame      || 
				function(callback){
					window.setTimeout(callback, 1000 / 60);
				};
			})();

			var tileSize = 32;

			/**
			 * Convert tile X and Y positions to pixels
			 * @param Int x
			 * @param Int y
			 * @return Object
			 */
			function translate(x, y)
			{
				return {
					x: Math.ceil(x * tileSize),
					y: Math.ceil(y * tileSize)
				}
			}

			/**
			 * Check if number is in range
			 * @param Int x
			 * @param Int min
			 * @param Int max
			 * @return Booelan
			 */
			function inRange(x, min, max)
			{
				return x >= min && x <= max;
			}

			// Declare global variables
			var Main, Game, Log, Classes = Entities = Renderers = {};

			// Define files to load
			var files = [
				<?php
					$tab = "";
					foreach($files as $stack)
					{
						foreach($stack as $file)
						{
							echo $tab."'".$file."',\n";

							// Some align stuff
							if(!$tab)
							{
								$tab = "				";
							}
						}
					}
				?>
			];

			// Load files and initialize system
			require(files, function()
			{
				Main = new Classes.Main();
				Game.start();
			});
		</script>
	</head>

	<body>
		<div id="game">
			<span id="top">
				<b>FPS:</b> <span id="fps_value">0</span> |
				<b>Mouse:</b> <span id="coordinates">(?,?)</span> |
				<a href="javascript:void(0)" onClick="Main.Loop.toggle(this)">Pause</a> |
				<a href="javascript:void(0)" onClick="Game.DevTools.toggleEditor(this)">Edit level</a> |
				<a href="javascript:void(0)" onClick="Game.Effects.toggleRain()">Toggle rain</a> |
				<a href="javascript:void(0)" onClick="Game.Effects.toggleSnow()">Toggle snow</a> |
				<a href="javascript:void(0)" onClick="Game.Effects.toggleNight()">Toggle night</a> |
				<a href="javascript:void(0)" onClick="Game.Effects.toggleThunder()">Toggle thunder</a> |
				<a href="javascript:void(0)" onClick="Game.Effects.toggleEarthquake()">Toggle earthquake</a>
			</span>

			<div id="canvases">
				<canvas id="background" width="800" height="480"></canvas>
				<canvas id="foreground" width="800" height="480"></canvas>
				<canvas id="entities" width="800" height="480"></canvas>
				<canvas id="ui" width="800" height="480"></canvas>
				<canvas id="dev" width="800" height="480"></canvas>
			</div>

			<div id="editor">
				<a style="background-color:#fff;float:right;width:128px;" href="javascript:void(0)" onClick="Game.DevTools.Editor.toggleSolid(this)">Type: block</a>
				<a style="background-color:#00baff" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(-1, this)">Air</a>
				<a style="background-color:#00baff" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(0, this)">Barrier</a>
				<a style="background-image:url(resources/grass.png)" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(1, this)" class="active"></a>
				<a style="background-image:url(resources/dirt.png)" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(2, this)"></a>
				<a style="background-image:url(resources/stone.png)" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(3, this)"></a>
				<a style="background-image:url(resources/sand.png)" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(4, this)"></a>
				<a style="background-image:url(resources/glass.png)" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(5, this)"></a>
				<a style="background-image:url(resources/wood.png)" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(6, this)"></a>
				<a style="background-image:url(resources/leaves.png)" href="javascript:void(0)" onClick="Game.DevTools.Editor.setType(7, this)"></a>
				<div style="clear:both;"></div>
			</div>
		</div>
	</body>
</html>