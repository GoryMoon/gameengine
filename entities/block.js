Entities.Block = Class.create({
	
	initialize: function(renderer, x, y, solid, type)
	{
		this.className = "Block";
		
		this.solid = solid;

		this.x = x;
		this.y = y;

		var coords = translate(x, y);
		this.realX = coords.x;
		this.realY = coords.y;

		this.type = type;
		this.renderer = renderer;
	},

	tick: function()
	{
		if(this.renderer.cleared)
		{
			if(!this.color)
			{
				this.invisible = false;

				switch(this.type)
				{
					case -2: this.image = "overlay"; break;
					case -1: this.invisible = true; this.solid = false; break;
					case 0: this.invisible = true; this.solid = true; break;
					case 1: this.image = "grass"; break;
					case 2: this.image = "dirt"; break;
					case 3: this.image = "stone"; break;
					case 4: this.image = "sand"; break;
					case 5: this.image = "glass"; break; // this.color = (Math.round(Math.random())) ? "rgba(206,239,242,0.4)" : "rgba(206,239,242,0.45)"; break;
					case 6: this.image = "wood"; break;
					case 7: this.image = "leaves"; break;
				}
			}
			
			var coords = translate(this.x + Game.x, this.y + Game.y);
			this.realX = coords.x;
			this.realY = coords.y;

			if(!this.invisible && Game.isVisible(this.x, this.y) && Game.Resources.isLoaded[this.image])
			{
				var self = this;

				this.renderer.queue(function(context)
				{
					context.drawImage(Game.Resources.images[self.image], self.realX, self.realY, tileSize, tileSize);
				});
			}
		}
	}
});