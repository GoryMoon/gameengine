Entities.Cloud = Class.create({
	
	initialize: function(x, y, vx, vy)
	{
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
	},

	tick: function()
	{
		this.x += this.vx;
		this.y += this.vy;

		if(this.x > 800 || this.y > 480)
		{
			this.x = Math.floor(Math.random() * -500);
			this.y = Math.floor(Math.random() * 200);
		}

		var self = this;

		Renderers.Background.queue(function(context)
		{
			context.fillStyle = "rgb(255,255,255)";
			context.fillRect(self.x, self.y, 100, 30);
			context.fillRect(self.x + 25, self.y - 20, 50, 30);
		});
	}
});