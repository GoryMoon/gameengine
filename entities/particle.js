Entities.Particle = Class.create({
	
	initialize: function(x, y, vx, vy, color, width, height)
	{
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		this.color = color;
		this.width = width;
		this.height = height;
	},

	tick: function()
	{
		this.x += this.vx;
		this.y += this.vy;

		if(this.x > 800 || this.y > 480)
		{
			this.x = Math.floor((Math.random() * 800) + 1);
			this.y = Math.floor(Math.random() * -480);
		}

		var self = this;

		Renderers.Entities.queue(function(context)
		{
			context.fillStyle = self.color;
			context.fillRect(self.x, self.y, self.width, self.height);
		});
	}
});