Entities.Player = Class.create({
	
	initialize: function(renderer, x, y)
	{
		this.name = "Jesper";
		
		this.className = "Player";
		
		this.renderer = renderer;
		this.solid = false;
		this.speed = 0.16;

		this.level = 1;
		this.xp = 0;
		this.maxXp = 100;

		this.scrollX = 0;
		this.scrollY = 0;

		this.staticX = x;
		this.staticY = y;

		// Runtime values
		this.isJumping = false;
		this.jumpHeight = 0;
		this.jumps = 0;
		this.isFalling = false;
		this.clearBackgroundNext = false;

		this.image = new Image();
		this.image.src = "resources/player.png";

		this.imageLoaded = false;
		
		this.frame = 0;

		var self = this;

		this.image.onload = function()
		{
			self.imageLoaded = true;
			Log.notice("resources/player.png has been loaded");
		};
	},

	/**
	 * Perform frame logic
	 */
	tick: function()
	{
		if(!Game.Effects.isEarthquake)
		{
			if(Main.Loop.difference < 100)
			{
				this.speed = Main.Loop.difference / 100;
				Game.Config.jumpSpeed = Main.Loop.difference / 80;
				Game.Config.fallSpeed = Main.Loop.difference / 80;
			}

			this.move();
		}

		if(this.imageLoaded)
		{
			this.draw();
		}
	},

	/**
	 * Move the player
	 */
	move: function()
	{
		this.x = this.staticX + this.scrollX;
		this.y = this.staticY + this.scrollY;

		// Jump
		if(Game.Input.jump && !this.isJumping && (!this.isFalling || this.jumps < Game.Config.jumps))
		{
			this.isJumping = true;
			this.isFalling = false;
			this.jumpHeight = 0;
			this.jumps++;
		}

		if(this.isJumping && this.jumpHeight < Game.Config.jumpHeight)
		{
			if(!Game.Collision.test(this, "up"))
			{
				this.jumpHeight += Game.Config.jumpSpeed;

				Game.y += Game.Config.jumpSpeed;
				this.scrollY -= Game.Config.jumpSpeed;
				Renderers.Background.cleared = true;
			}
			else
			{
				this.isJumping = false;
				this.isFalling = true;
			}
		}
		else if(this.isJumping && this.jumpHeight >= Game.Config.jumpHeight)
		{
			this.isJumping = false;
			this.isFalling = true;
		}

		// Gravity
		if(!this.isJumping)
		{
			if(!Game.Collision.test(this, "down"))
			{
				this.isFalling = true;
				Game.y -= Game.Config.fallSpeed;
				this.scrollY += Game.Config.fallSpeed;

				this.clearBackgroundNext = true;

				Renderers.Background.cleared = true;
			}
			else
			{
				this.isFalling = false;
				this.jumps = 0;
			}
		}
		else
		{
			this.isFalling = false;
		}

		// Go right
		if(Game.Input.right)
		{
			if(this.frame != 1)
			{
				this.frame = 1;
				Renderers.Entities.cleared = true;
			}

			if(!Game.Collision.test(this, "right"))
			{
				Game.x -= this.speed;
				this.scrollX += this.speed;
				Renderers.Background.cleared = true;
			}
		}

		// Go left
		else if(Game.Input.left)
		{
			if(this.frame != 2)
			{
				this.frame = 2;
				Renderers.Entities.cleared = true;
			}

			if(!Game.Collision.test(this, "left"))
			{
				Game.x += this.speed;
				this.scrollX -= this.speed;
				Renderers.Background.cleared = true;
			}
		}

		// Stand still
		else
		{
			if(this.frame != 0)
			{
				this.frame = 0;
				Renderers.Entities.cleared = true;
			}
		}

		if(this.clearBackgroundNext)
		{
			Renderers.Background.cleared = true;
		}
	},

	/**
	 * Draw the player if necessary
	 */
	draw: function()
	{
		if(this.renderer.cleared)
		{
			var coords = translate(this.staticX, this.staticY);
			this.realX = coords.x;
			this.realY = coords.y;

			var self = this;

			this.renderer.queue(function(context)
			{
				context.drawImage(self.image, self.frame * 32, 0, tileSize, tileSize, self.realX, self.realY, tileSize, tileSize);
			});
		}
	}
});